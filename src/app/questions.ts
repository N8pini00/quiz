export class Question {
    question: string;
    options: string[];
    correctOption: number;
}
