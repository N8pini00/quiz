import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

    constructor(public router: Router, public quizService: QuizService) {}

    public feedback: string;
    public duration: number;
    public correctCounter: number;

    ngOnInit() {
      this.quizService.addQuestions();
      this.quizService.initialize();
      this.feedback = '';
    }

    public checkOption(option: number) {
      this.quizService.setOptionSelected();
      if (this.quizService.areAllOptionsUsed() ) {
        this.quizService.setQuestion();
      }
      else {
        if (this.quizService.isCorrectOption(option)) {
          this.feedback =
            this.quizService.getCorrectOptionOfActiveQuestion() +
          '    - true! Good work.';
        }
        else {
          this.feedback = 'Wrong. The answer is ' +
          (this.quizService.getCorrectOptionOfActiveQuestion());
        }
      }
    }
    public continue() {
      if (this.quizService.isFinished()) {
        this.duration = this.quizService.getDuration();
        this.router.navigateByUrl('results/');
      }
      else {
        this.quizService.setQuestion();
      }
    }

  }
