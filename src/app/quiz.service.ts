import { Injectable } from '@angular/core';
import { Question } from '../app/questions';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: Question[] = [];
  private activeQuestion: Question;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  public correctCounter: number;
  public duration: number;
  public durationSeconds: number;
  public speed: string;
  public correctness: number;

  constructor(public router: Router,
              public activatedRoute: ActivatedRoute) { }

  public addQuestions() {
    this.questions = [{
        question: 'Which film is Elsa & Anna starring?',
        options: [
        'Frozen',
        'Freezing',
        'Froosh'],
      correctOption: 0
    },
    {
      question: 'Which of these exist?',
      options: [
        'Star Wars',
        'Comet Wars',
        'Dynamite Wars'],
      correctOption: 0
    },
    {
      question: 'Which one is a Disney pet dog?',
      options: [
        'Blackie',
        'Pluto',
        'Rudolph'],
      correctOption: 1
    },
    {
      question: 'Which of these is a Disney couple?',
      options: [
        'Goofy and Olga',
        'Donald and Amber Duck',
        'Mickey and Minnie Mouse'],
      correctOption: 2
    }
    ];
  }

  public setQuestion() {
    this.optionCounter = 0;
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
    this.correctCounter++;
  }

  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
    this.correctCounter = 0;
  }

  public getQuestions(): Question[] {
    return this.questions;
  }

  public getActiveQuestion(): Question {
    return this.activeQuestion;
  }

  public getQuoteOfActiveQuestion(): string {
    return this.activeQuestion.question;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfCorrectAnswers(): number {
    return this.correctCounter;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter() {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++;
    return (this.optionCounter > 1)
    ? true : false;
  }

  public isCorrectOption(option: number) {
    this.isCorrect =
      (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length)
    ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

  public Duration(){
  this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
  this.durationSeconds = Math.round((this.duration) / 1000);
  }


  public getSpeed() {
    if (this.duration < 7000) {
    return this.speed = 'Lighting fast!';
  } else if (this.duration < 10000) {
      return this.speed = 'Wow, that was fast!';
  } else {
    return this.speed = 'Good job!';
  }
  }

  public getCorrectness() {
    this.correctness = 100 * this.correctCounter / this.questionCounter;
    return this.correctness;
  }

}

