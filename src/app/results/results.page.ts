import { Component, OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { NumericValueAccessor } from '@ionic/angular';
import { QuizService } from '../quiz.service';
// import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage {

  constructor(public quizService: QuizService) {}

}
